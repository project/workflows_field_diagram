<?php

namespace Drupal\workflows_field_diagram\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Entity\Workflow;

/**
 * Plugin implementation of the 'Workflows Field Diagram Formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "workflows_field_diagram_formatter",
 *   label = @Translation("Workflows Field Diagram"),
 *   field_types = {
 *     "workflows_field_item"
 *   }
 * )
 */
class WorkflowsFieldDiagramFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'flowchart-orientation' => 'LR',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['flowchart-orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Mermaid Flowchart Orientation'),
      '#description' => $this->t('This defines the direction of the Flowchart'),
      '#options' => [
        'TB' => $this->t('TB - top to bottom'),
        'BT' => $this->t('BT - bottom to top'),
        'RL' => $this->t('RL - right to left'),
        'LR' => $this->t('LR - left to right'),
      ],
      '#default_value' => $this->getSetting('flowchart-orientation'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Mermaid Flowchart Orientation: @flowchart-orientation', ['@flowchart-orientation' => $this->getSetting('flowchart-orientation')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // Load workflow.
    $workflow_id = $this->fieldDefinition->getSetting('workflow');
    $workflow = Workflow::load($workflow_id);
    $workflowType = $workflow->getTypePlugin();
    $states = $workflowType->getStates();

    foreach ($items as $delta => $item) {

      // Mark active state.
      $active_state_id = $item->value;

      // Pass classes to states.
      $classes = [
        'states' => [
          $active_state_id => ['active-state'],
        ],
        'transitions' => [],
      ];

      // Disable states or transitions,.
      $disabled = [
        'states' => [],
        'transitions' => [],
      ];

      $element[$delta] = [
        '#theme' => 'workflows_diagram',
        '#workflow' => $workflow,
        '#classes' => $classes,
        '#disabled' => $disabled,
        '#orientation' => $this->getSetting('flowchart-orientation'),
        '#attached' => [
          'library' => [
            'workflows_field_diagram/workflows_field_diagram',
          ],
        ],
      ];
    }

    return $element;
  }

}
